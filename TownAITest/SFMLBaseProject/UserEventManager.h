#pragma once
#include "ChopTreeEvent.h"

class UserEventManager : IUpdatable, IDrawable, IObserver
{
private:
	IUserEvent * _currentEvent;


public:
	UserEventManager();
	~UserEventManager();

	void CreateUserEvent(IUserEvent* newEvent);

	void Update(float elapsedTime) override;
	void Draw() override;

	void OnNotify(Message* recievedMessage) override;
};

