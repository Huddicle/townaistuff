#pragma once
#include "JobErrorLog.h"

class JobQueue : public IUpdatable
{
private:
	std::vector<GroupJob*> _currentJobs;

	static JobQueue* _instance;
public:
	JobQueue();
	~JobQueue();

	static JobQueue* Instance();

	void AddJobToQueue(GroupJob* newJob);

	void Update(float t) override;
};

