#pragma once

class Message;

class IObserver
{
private:

public:
	IObserver() {};
	virtual ~IObserver() {};

	virtual void OnNotify(Message* message) = 0;
};