#ifndef _IENTITY_H
#define _IENTITY_H

class IEntity
{
private:

public:
	IEntity() {};
	~IEntity() {};

	virtual void AddToHunger(float toAdd) = 0;
	virtual void AddToEnergy(float toAdd) = 0;
};

#endif