#include "Game.h"



Game::Game(int argc, char* argv[])
{
	srand(time(NULL));
	_gameWindow = new RenderWindow(VideoMode(1280, 720), "THING");
	
	Clock clock;

	LoadContent();

	while (_gameWindow->isOpen())
	{
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (_gameWindow->pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
				_gameWindow->close();
		}

		Update(clock.restart().asMilliseconds());
		Draw();

	}
}


Game::~Game()
{
}

void Game::LoadContent()
{
	GraphicsManager::Instance()->Init(_gameWindow);
	CollisionManager::Instance()->Init(_gameWindow);
	CursorManager::Instance()->Init(_gameWindow);

	_testPerson = new Entity();

	_map = new Map();
	_map->Initialise();

	JobFactory::Instance()->Init(_map);

	_UI = new UI(_gameWindow);
}

void Game::Update(int elapsedTime)
{
	_testPerson->Update(elapsedTime);

	InputManager::Instance()->Update(elapsedTime);
	_UI->Update(elapsedTime);
}

void Game::Draw()
{
	GraphicsManager::Instance()->Clear(0.5f, 0.5f, 0.5f);

	_map->Draw();
	_testPerson->Draw();
	_UI->Draw();
	CursorManager::Instance()->Draw();

	_gameWindow->display();
}