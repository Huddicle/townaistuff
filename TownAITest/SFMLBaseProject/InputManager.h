#pragma once
#include "SFMLFactory.h"

class InputManager : public IUpdatable, public Subject
{
private:
	bool _previousMouseDown;
	Vector2i _previousMousePosition;

	InputManager();

	static InputManager* _instance;

public:
	~InputManager();

	static InputManager* Instance();

	void Update(float t) override;
};

