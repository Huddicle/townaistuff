#ifndef _GRAPHICSMANAGER_H
#define _GRAPHICSMANAGER_H

#include "CollisionManager.h"

class GraphicsManager
{
private:
	static GraphicsManager * _instance;

	RenderWindow* _gameWindow;

	Font _font;

public:
	GraphicsManager();
	~GraphicsManager();

	static GraphicsManager* Instance();

	void Init(RenderWindow* gameWindow);
	
	void Clear(float r, float g, float b);
	void DrawSprite(const Sprite& sprite);
	void DrawRectangle(const RectangleShape& rect);

	void PrintToScreen(const sf::String& text, const Vector2f& position);


};

#endif