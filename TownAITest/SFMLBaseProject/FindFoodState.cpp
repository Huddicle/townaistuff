#include "FindFoodState.h"

FindFoodState* FindFoodState::_instance = nullptr;

FindFoodState::FindFoodState()
{
}


FindFoodState::~FindFoodState()
{
}

FindFoodState * FindFoodState::Instance()
{
	if (_instance == nullptr)
	{
		_instance = new FindFoodState();
	}

	return _instance;
}

void FindFoodState::Enter()
{
	std::cout << "Going to find food" << std::endl;
}

void FindFoodState::Execute(IEntity * entity, float t)
{
	entity->AddToHunger(0.02f);
	entity->AddToEnergy(-0.02f);
	std::cout << "Finding food..." << std::endl;
}

void FindFoodState::Exit()
{
	std::cout << "Finished finding food" << std::endl;
}
