#pragma once
#include "InputManager.h"

class CollisionManager
{
private:
	static CollisionManager* _instance;

	RenderWindow* _gameWindow;
public:
	CollisionManager();
	~CollisionManager();

	static CollisionManager* Instance();

	Vector2i GetMousePosition() { return Mouse::getPosition(*_gameWindow); }

	void Init(RenderWindow* gameWindow);

	bool IsMouseColliding(const IntRect& rect);

	bool IsBoxColliding(float x1, float y1, float w1, float h1, float x2, float y2, float w2, float h2);
};

