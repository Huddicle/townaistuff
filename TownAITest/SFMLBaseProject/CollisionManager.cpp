#include "CollisionManager.h"

CollisionManager* CollisionManager::_instance = nullptr;

CollisionManager::CollisionManager()
{
}


CollisionManager::~CollisionManager()
{
}

CollisionManager * CollisionManager::Instance()
{
	if (_instance == nullptr)
	{
		_instance = new CollisionManager();
	}

	return _instance;
}

void CollisionManager::Init(RenderWindow * gameWindow)
{
	_gameWindow = gameWindow;
}

bool CollisionManager::IsMouseColliding(const IntRect & rect)
{
	Vector2i mousePos = Mouse::getPosition(*_gameWindow);

	if (mousePos.x < rect.left) return false;
	if (mousePos.x > rect.left + rect.width) return false;
	if (mousePos.y < rect.top) return false;
	if (mousePos.y > rect.top + rect.height) return false;
	return true;
}

bool CollisionManager::IsBoxColliding(float x1, float y1, float w1, float h1, float x2, float y2, float w2, float h2)
{
	if (x1 + w1 < x2 || x1 > x2 + h2) return false;
	if (y1 + h1 < y2 || y1 > y2 + h2) return false;

	return true;
}
