#pragma once
#include "SFML\Graphics.hpp"
#include "SFML\Window.hpp"
#include "SFML\System.hpp"
#include "SFML\Audio.hpp"
#include "IUpdatable.h"
#include "IDrawable.h"
#include "IUI.h"
#include "IState.h"
#include "IEntity.h"
#include "IUserEvent.h"
#include "IObserver.h"
#include <iostream>
#include <string>
#include <vector>
#include <Windows.h>

using namespace sf;

class IMapObject;

//ENTITY JOBS
enum class SINGLE_JOB_TYPE
{
	CHOP_TREE,
	MINE_ROCKS,
	BUILD_WALL,
	BUILD_FLOOR,
	RUN_AWAY,
	FIGHT
};

struct SingleJob
{
	SINGLE_JOB_TYPE type;
	int priority;
};

enum GROUP_JOB_TYPE
{
	CHOP_TREES,
	MINE_ROCKS,
	BUILD_ROOM,
};

struct GroupJob
{
	std::vector<SingleJob*> jobsToDo;
	GROUP_JOB_TYPE type;
};

struct DestroyItemJob : SingleJob
{
	IMapObject* item;
};

//EVENT MESSAGES
enum MESSAGE_TYPE
{
	INPUT_MESSAGE,
};

enum INPUT_MESSAGE_TYPE
{
	MOUSE_DOWN,
	MOUSE_UP,
	MOUSE_CLICK,
	KEY_DOWN,
	KEY_UP
};

struct Message
{
	MESSAGE_TYPE type;
};

struct InputMessage : public Message
{
	INPUT_MESSAGE_TYPE inputType;
};


/*
TODO LIST:

- Get Event to add job to job queue
- Get Map to find objects on a certain spot for destruction


*/