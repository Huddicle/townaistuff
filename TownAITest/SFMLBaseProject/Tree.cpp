#include "Tree.h"



Tree::Tree()
{
	_type = MAPOBJECTTYPE::TREE;
	_sprite = SFMLFactory::Instance()->MakeSprite("Resources/Tree.png");
}


Tree::~Tree()
{
}

void Tree::Update(float t)
{
}

void Tree::Draw()
{
	GraphicsManager::Instance()->DrawSprite(*_sprite);
}

void Tree::Damage()
{
}

void Tree::SetPosition(float x, float y)
{
	_sprite->setPosition(x, y);
}
