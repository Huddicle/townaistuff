#include "SFMLFactory.h"

SFMLFactory* SFMLFactory::_instance = nullptr;

SFMLFactory::SFMLFactory()
{
}


SFMLFactory::~SFMLFactory()
{
}

SFMLFactory * SFMLFactory::Instance()
{
	if (_instance == nullptr)
	{
		_instance = new SFMLFactory();
	}

	return _instance;
}

Sprite * SFMLFactory::MakeSprite(const char * texturePath)
{
	Texture* texture = new Texture();
	texture->loadFromFile(texturePath);
	Sprite* sprite = new Sprite(*texture);
	return sprite;
}
