#include "GraphicsManager.h"

GraphicsManager* GraphicsManager::_instance = nullptr;

GraphicsManager::GraphicsManager()
{
	_font = sf::Font();
	_font.loadFromFile("Resources/Vera.ttf");
}


GraphicsManager::~GraphicsManager()
{
}

GraphicsManager * GraphicsManager::Instance()
{
	if (_instance == nullptr)
	{
		_instance = new GraphicsManager();
	}

	return _instance;
}

void GraphicsManager::Init(RenderWindow * gameWindow)
{
	_gameWindow = gameWindow;
}

void GraphicsManager::Clear(float r, float g, float b)
{
	sf::Color color = sf::Color(r * 255, g * 255, b * 255, 255);
	_gameWindow->clear(color);
}

void GraphicsManager::DrawSprite(const Sprite & sprite)
{
	_gameWindow->draw(sprite);
}

void GraphicsManager::DrawRectangle(const RectangleShape & rect)
{
	_gameWindow->draw(rect);
}

void GraphicsManager::PrintToScreen(const sf::String & text, const Vector2f & position)
{
	Text textToDisplay = Text(text, _font, 14);
	textToDisplay.setPosition(position);
	_gameWindow->draw(textToDisplay);
}
