#pragma once
#include "Commons.h"

enum MAPOBJECTTYPE
{
	TREE,
	MAXTYPES,
};

class IMapObject : public IUpdatable, public IDrawable
{
protected:
	MAPOBJECTTYPE _type;
	float _health;
public:
	IMapObject() {};
	~IMapObject() {};


	virtual void Update(float t) override = 0;
	virtual void Draw() override = 0;

	virtual void Damage() = 0;

	virtual const Vector2f GetPosition() const = 0;
	virtual const Vector2f GetSize() const = 0;

	MAPOBJECTTYPE GetObjectType() { return _type; }

};

