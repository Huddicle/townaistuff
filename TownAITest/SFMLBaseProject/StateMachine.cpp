#include "StateMachine.h"



StateMachine::StateMachine(IEntity* owner) : _owner(owner)
{
}


StateMachine::~StateMachine()
{
}

void StateMachine::ChangeState(IState * newState)
{
	if (_currentState != nullptr)
	{
		_currentState->Exit();
	}
	_previousState = _currentState;
	
	_currentState = newState;
	if (_currentState != nullptr)
	{
		_currentState->Enter();
	}
}

void StateMachine::Update(float t)
{
	if (_owner != nullptr)
	{
		if (_currentState != nullptr)
		{
			_currentState->Execute(_owner, t);
		}
	}
}
