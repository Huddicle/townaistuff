#ifndef _STATEMACHINE_H
#define _STATEMACHINE_H

#include "RestingState.h"

class StateMachine : IUpdatable
{
private:
	IEntity* _owner;

	IState* _previousState;
	IState* _currentState;


public:
	StateMachine(IEntity* owner);
	~StateMachine();

	void ChangeState(IState* newState);

	void Update(float t) override;

};

#endif