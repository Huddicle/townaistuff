#pragma once
#include "UIButton.h"

class UI : public IUI, public IObserver
{
private:
	static UserEventManager* _userEventManager;

	static void SetUpChopTreeEvent();

	std::vector<UIButton*> _buttons;

public:
	UI(RenderWindow* window);
	~UI();

	std::vector<UIButton*> GetButtons() override;
	void AddButton(UIButton* button) override;
	void Update(float t) override;
	void Draw() override;

	void OnNotify(Message* message) override;


};

