#ifndef _FINDFOODSTATE_H
#define _FINDFOODSTATE_H

#include "UI.h"

class FindFoodState : public IState
{
private:
	static FindFoodState * _instance;
public:
	FindFoodState();
	~FindFoodState();

	static FindFoodState* Instance();

	void Enter() override;
	void Execute(IEntity* entity, float t) override;
	void Exit() override;

};

#endif