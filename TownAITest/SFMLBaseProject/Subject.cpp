#include "Subject.h"



Subject::Subject()
{
}


Subject::~Subject()
{
}

void Subject::Subscribe(IObserver * newObserver)
{
	_observers.push_back(newObserver);
}

void Subject::Notify(Message* messageToSend)
{
	for (IObserver* observer : _observers)
	{
		observer->OnNotify(messageToSend);
	}
}
