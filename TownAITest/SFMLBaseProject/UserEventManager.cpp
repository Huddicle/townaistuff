#include "UserEventManager.h"



UserEventManager::UserEventManager()
{
	InputManager::Instance()->Subscribe(this);
}


UserEventManager::~UserEventManager()
{
}

void UserEventManager::CreateUserEvent(IUserEvent* newEvent)
{
	if (_currentEvent != nullptr)
	{
		_currentEvent->Cancel();
	}
	delete _currentEvent;
	_currentEvent = newEvent;
}

void UserEventManager::Update(float elapsedTime)
{
	if (_currentEvent != nullptr)
	{
		_currentEvent->Update(elapsedTime);
	}
}

void UserEventManager::Draw()
{
	if (_currentEvent != nullptr)
	{
		_currentEvent->Draw();
	}
}

void UserEventManager::OnNotify(Message * recievedMessage)
{
	if (recievedMessage->type == MESSAGE_TYPE::INPUT_MESSAGE)
	{
		InputMessage* message = static_cast<InputMessage*>(recievedMessage);

		switch (message->inputType)
		{
		case INPUT_MESSAGE_TYPE::MOUSE_DOWN:
			if (_currentEvent != nullptr) _currentEvent->Start();
			break;
		case INPUT_MESSAGE_TYPE::MOUSE_UP:
			if (_currentEvent != nullptr)
			{
				if (_currentEvent->HasStarted())
				{
					_currentEvent->Finish();
					delete _currentEvent;
					_currentEvent = nullptr;
				}
			}
			break;
		case INPUT_MESSAGE_TYPE::MOUSE_CLICK:

			break;
		}
	}
}
