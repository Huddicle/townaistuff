#include "JobQueue.h"

JobQueue* JobQueue::_instance = nullptr;

JobQueue::JobQueue()
{
}


JobQueue::~JobQueue()
{
}

JobQueue * JobQueue::Instance()
{
	if (_instance == nullptr)
	{
		_instance = new JobQueue();
	}

	return _instance;
}

void JobQueue::AddJobToQueue(GroupJob * newJob)
{
	if (newJob != nullptr)
	{
		_currentJobs.push_back(newJob);
	}
}

void JobQueue::Update(float t)
{
	
}
