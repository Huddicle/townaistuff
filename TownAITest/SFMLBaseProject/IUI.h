#pragma once

class UIButton;

class IUI : public IUpdatable, public IDrawable
{
private:
public:
	IUI() {};
	~IUI() {};

	virtual std::vector<UIButton*> GetButtons() = 0;
	virtual void AddButton(UIButton* button) = 0;
	virtual void Update(float t) override = 0;
	virtual void Draw() override = 0;
};

