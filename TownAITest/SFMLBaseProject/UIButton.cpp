#include "UIButton.h"



UIButton::UIButton(Sprite* sprite, Vector2f position, void(callbackMethod)()) : _sprite(sprite), _position(position), _callbackMethod(callbackMethod)
{
	if (sprite != nullptr)
	{
		_sprite->setPosition(position);
		_textureRect = new IntRect(0, 0, _sprite->getTexture()->getSize().x / 2, _sprite->getTexture()->getSize().y);
		_sprite->setTextureRect(*_textureRect);
	}
}

UIButton::~UIButton()
{
}

void UIButton::Update(float t)
{
}

void UIButton::Draw()
{
	_textureRect->left = _textureRect->width * _animationStage;
	_sprite->setTextureRect(*_textureRect);
	GraphicsManager::Instance()->DrawSprite(*_sprite);
}

void UIButton::OnMouseDown()
{
	if (CollisionManager::Instance()->IsMouseColliding(IntRect(GetPosition().x, GetPosition().y, GetWidth(), GetHeight())))
	{
		_animationStage = 1;
	}
}

void UIButton::OnMouseUp()
{
	if (_animationStage == 1)
	{
		if (CollisionManager::Instance()->IsMouseColliding(IntRect(GetPosition().x, GetPosition().y, GetWidth(), GetHeight())))
		{
			//_callbackMethod();
			if (_callbackMethod != nullptr)
			{
				_callbackMethod();
			}
			std::cout << "Click" << std::endl;
		}
	}
	_animationStage = 0;
}

void UIButton::OnMouseClick()
{
}
