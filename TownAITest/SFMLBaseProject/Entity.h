#ifndef _ENTITY_H
#define _ENTITY_H

#include "StateMachine.h"

class Entity : public IDrawable, public IUpdatable, public IEntity
{
private:
	StateMachine * _stateMachine;

	float _health;
	float _hunger;
	float _energy;
public:
	Entity();
	~Entity();

	void Update(float t) override;
	void Draw() override;

	void AddToHunger(float toAdd) override { _hunger += toAdd; }
	void AddToEnergy(float toAdd) override { _energy += toAdd; }

};

#endif