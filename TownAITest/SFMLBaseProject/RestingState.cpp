#include "RestingState.h"

RestingState* RestingState::_instance = nullptr;

RestingState::RestingState()
{
}


RestingState::~RestingState()
{
}

RestingState * RestingState::Instance()
{
	if (_instance == nullptr)
	{
		_instance = new RestingState();
	}

	return _instance;
}

void RestingState::Enter()
{
	std::cout << "Gonna go for a rest" << std::endl;
}

void RestingState::Execute(IEntity * entity, float t)
{
	entity->AddToEnergy(0.02f);
	std::cout << "Resting..." << std::endl;
}

void RestingState::Exit()
{
	std::cout << "Finished resting" << std::endl;
}
