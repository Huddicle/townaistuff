#pragma once
#include "Subject.h"

class SFMLFactory
{
private:
	static SFMLFactory* _instance;
public:
	SFMLFactory();
	~SFMLFactory();

	static SFMLFactory* Instance();

	Sprite* MakeSprite(const char* texturePath);
};

