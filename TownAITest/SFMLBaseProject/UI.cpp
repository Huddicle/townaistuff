#include "UI.h"

UserEventManager* UI::_userEventManager = nullptr;

UI::UI(RenderWindow* window)
{
	InputManager::Instance()->Subscribe(this);
	_userEventManager = new UserEventManager();

	Sprite* woodSprite = SFMLFactory::Instance()->MakeSprite("Resources/AxeButton.png");
	UIButton* collectWoodButton = new UIButton(woodSprite, Vector2f(100.0f, 400.0f), SetUpChopTreeEvent);

	_buttons.push_back(collectWoodButton);
}


UI::~UI()
{
}

std::vector<UIButton*> UI::GetButtons()
{
	return _buttons;
}

void UI::AddButton(UIButton * button)
{
	if (button != nullptr)
	{
		_buttons.push_back(button);
	}
}

void UI::Update(float t)
{
	for (UIButton* button : _buttons)
	{
		button->Update(t);
	}
	_userEventManager->Draw();
}

void UI::Draw()
{
	for (UIButton* button : _buttons)
	{
		button->Draw();
	}
	_userEventManager->Draw();
}

void UI::OnNotify(Message * recievedMessage)
{
	if (recievedMessage->type == MESSAGE_TYPE::INPUT_MESSAGE)
	{
		InputMessage* message = static_cast<InputMessage*>(recievedMessage);

		switch (message->inputType)
		{
		case INPUT_MESSAGE_TYPE::MOUSE_DOWN:
			for (UIButton* button : _buttons)
			{
				button->OnMouseDown();
			}
			break;
		case INPUT_MESSAGE_TYPE::MOUSE_UP:
			for (UIButton* button : _buttons)
			{
				button->OnMouseUp();
			}
			break;
		case INPUT_MESSAGE_TYPE::MOUSE_CLICK:
			for (UIButton* button : _buttons)
			{
				if (CollisionManager::Instance()->IsMouseColliding(IntRect(button->GetPosition().x, button->GetPosition().y, button->GetWidth(), button->GetHeight())))
				{
					button->OnMouseClick();
				}
			}
			break;
		}
	}
}

void UI::SetUpChopTreeEvent()
{
	CursorManager::Instance()->SetCursorType(CURSOR_TYPES::CHOP_TREE_CURSOR);
	_userEventManager->CreateUserEvent(new ChopTreeEvent());
	
}
