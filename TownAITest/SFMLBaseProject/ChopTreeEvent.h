#pragma once
#include "JobFactory.h"

class ChopTreeEvent : public IUserEvent
{
private:
	Vector2f _startPosition;
	Vector2f _endPosition;
public:
	ChopTreeEvent();
	~ChopTreeEvent();

	void Start() override;
	void Cancel() override;
	void Finish() override;

	void Update(float t) override;
	void Draw() override;
};

