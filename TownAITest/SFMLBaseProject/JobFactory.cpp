#include "JobFactory.h"

JobFactory* JobFactory::_instance = nullptr;

JobFactory::JobFactory()
{
}


JobFactory::~JobFactory()
{
}

void JobFactory::Init(Map* map)
{
	_gameMap = map;
}

JobFactory * JobFactory::Instance()
{
	if (_instance == nullptr)
	{
		_instance = new JobFactory();
	}

	return _instance;
}

void JobFactory::CreateChopTreeGroupJob(const Vector2f & startPosition, const Vector2f & endPosition)
{
	float minX;
	float maxX;
	if (startPosition.x < endPosition.x)
	{
		minX = startPosition.x;
		maxX = endPosition.x;
	}
	else
	{
		minX = endPosition.x;
		maxX = startPosition.x;
	}

	float minY;
	float maxY;
	if (startPosition.y < endPosition.y)
	{
		minY = startPosition.y;
		maxY = endPosition.y;
	}
	else
	{
		minY = endPosition.y;
		maxY = startPosition.y;
	}

	float width = maxX - minX;
	float height = maxY - minY;

	std::vector<IMapObject*> treesToCut = _gameMap->GetAllObjectsOfTypeInArea(MAPOBJECTTYPE::TREE, Vector2f(minX, minY), Vector2f(width, height));


}
