#ifndef _GAME_H
#define _GAME_H
#include "Entity.h"


class Game
{
private:
	RenderWindow* _gameWindow;

	UI* _UI;

	Entity* _testPerson;

	UIButton* _collectWoodButton;

	Map* _map;
public:
	Game(int argc, char* argv[]);
	~Game();

	void LoadContent();
	void Update(int elapsedTime);
	void Draw();
};

#endif