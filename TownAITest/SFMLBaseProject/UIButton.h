#pragma once
#include "UserEventManager.h"

class UI;

class UIButton : public IUpdatable, public IDrawable
{

	Vector2f _position;
	//Method to call when clicked;
	void(*_callbackMethod) ();
	Sprite* _sprite;
	IntRect* _textureRect;
	int _animationStage;
public:
	UIButton(Sprite* sprite = nullptr, Vector2f position = Vector2f(0.0f, 0.0f), void(callbackMethod)() = nullptr);
	~UIButton();

	void Update(float t) override;
	void Draw() override;

	void SetSprite(Sprite* newSprite)					{ if (newSprite != nullptr) { delete _sprite; _sprite = newSprite; } }
	void SetCallback(void(*newFunction)())				{ if (_callbackMethod != nullptr) _callbackMethod = newFunction; }

	Vector2f GetPosition()								{ return _position; }
	float GetWidth()									{ return _textureRect->width; }
	float GetHeight()									{ return _textureRect->height;  }

	virtual void OnMouseDown();
	virtual void OnMouseUp();
	virtual void OnMouseClick();
};

