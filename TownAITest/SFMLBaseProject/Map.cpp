#include "Map.h"



Map::Map()
{
}


Map::~Map()
{
}

void Map::Initialise()
{
	for (int i = 0; i < 10; i++)
	{
		int newObjectType = std::floor(rand()%MAPOBJECTTYPE::MAXTYPES);

		switch (newObjectType)
		{
		case MAPOBJECTTYPE::TREE:
			Tree* tree = new Tree();
			tree->SetPosition((rand() % 500) + 300, (rand() % 500) + 300);
			_mapObjects.push_back(tree);
			break;
		}

	}
}

void Map::Update(float t)
{
}

void Map::Draw()
{
	for (auto object : _mapObjects)
	{
		object->Draw();
	}
}

std::vector<IMapObject*> Map::GetAllObjectsOfType(MAPOBJECTTYPE type)
{
	std::vector<IMapObject*> returnVector = std::vector<IMapObject*>();
	for (IMapObject* object : _mapObjects)
	{
		if (object->GetObjectType() == type)
		{
			returnVector.push_back(object);
		}
	}
	return returnVector;
}

std::vector<IMapObject*> Map::GetAllObjectsOfTypeInArea(MAPOBJECTTYPE type, Vector2f & position, Vector2f & areaSize)
{
	std::vector<IMapObject*> returnVector = std::vector<IMapObject*>();
	for (IMapObject* object : _mapObjects)
	{
		if (object->GetObjectType() == type)
		{
			Vector2f objectPosition = object->GetPosition();
			Vector2f objectSize = object->GetSize();
 			if (CollisionManager::Instance()->IsBoxColliding(objectPosition.x, objectPosition.y, objectSize.x, objectSize.y, position.x, position.y, areaSize.x, areaSize.y))
			{
				returnVector.push_back(object);
			}

		}
	}
	return returnVector;
}
