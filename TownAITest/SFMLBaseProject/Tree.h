#pragma once
#include "JobQueue.h"

class Tree : public IMapObject
{
private:
	Sprite * _sprite;
public:
	Tree();
	~Tree();

	virtual void Update(float t) override;
	virtual void Draw() override;

	virtual void Damage() override;

	const Vector2f GetPosition() const override				{ return _sprite->getPosition(); }
	const Vector2f GetSize() const override					{ return Vector2f(_sprite->getTextureRect().width, _sprite->getTextureRect().height); }

	void SetPosition(float x, float y);
};

