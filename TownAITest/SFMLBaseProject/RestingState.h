#ifndef _RESTINGSTATE_H
#define _RESTINGSTATE_H

#include "FindFoodState.h"


class RestingState : public IState
{
private:
	static RestingState * _instance;
public:
	RestingState();
	~RestingState();

	static RestingState* Instance();

	void Enter() override;
	void Execute(IEntity* entity, float t) override;
	void Exit() override;

};

#endif