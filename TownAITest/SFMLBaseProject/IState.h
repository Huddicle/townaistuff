#ifndef _ISTATE_H
#define _ISTATE_H

class IEntity;

class IState
{
private:

public:
	IState();
	~IState();
	
	virtual void Enter() = 0;
	virtual void Execute(IEntity* entity, float t) = 0;
	virtual void Exit() = 0;
};

#endif