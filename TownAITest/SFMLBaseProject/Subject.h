#pragma once
#include "IMapObject.h"

class Subject
{
private:
	std::vector<IObserver*> _observers;
public:
	Subject();
	~Subject();

	void Subscribe(IObserver* newObserver);
	void Notify(Message* messageToSend);
};

