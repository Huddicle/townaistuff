#ifndef _IDRAWABLE_H
#define _IDRAWABLE_H

class IDrawable
{
public:
	IDrawable() {};
	~IDrawable() {};

	virtual void Draw() = 0;
};

#endif