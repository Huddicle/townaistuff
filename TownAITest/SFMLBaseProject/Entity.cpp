#include "Entity.h"



Entity::Entity() : _health(100.0f), _hunger (100.0f), _energy(100.0f)
{
	_stateMachine = new StateMachine(this);
}


Entity::~Entity()
{
}

void Entity::Update(float t)
{
	_stateMachine->Update(t);

	if (_hunger < 25.0f)
	{
		_stateMachine->ChangeState(FindFoodState::Instance());
	}
	if (_energy < 25.0f)
	{
		_stateMachine->ChangeState(RestingState::Instance());
	}

	_hunger -= 0.01f;



}

void Entity::Draw()
{
	RectangleShape rect = RectangleShape(Vector2f(10.0f, 10.0f));
	rect.setFillColor(sf::Color(_hunger * 2.5f, 0.0f, _energy * 2.5f));
	rect.setPosition(Vector2f(200.0f, 200.0f));
	GraphicsManager::Instance()->DrawRectangle(rect);


	GraphicsManager::Instance()->PrintToScreen( "Hunger: " + std::to_string(_hunger), Vector2f(50.0f, 50.0f));
	GraphicsManager::Instance()->PrintToScreen("Energy: " + std::to_string(_energy), Vector2f(50.0f, 100.0f));
}


// TODO::Display print hunger and energy to check stats and see if state change actually should trigger or not