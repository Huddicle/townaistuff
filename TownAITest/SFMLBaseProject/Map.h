#pragma once
#include "Tree.h"

class Map : public IUpdatable, public IDrawable
{
private:
	std::vector<IMapObject*> _mapObjects;
public:
	Map();
	~Map();

	void Initialise();


	void Update(float t) override;
	void Draw() override;

	std::vector<IMapObject*> GetAllObjectsOfType(MAPOBJECTTYPE type);
	std::vector<IMapObject*> GetAllObjectsOfTypeInArea(MAPOBJECTTYPE type, Vector2f& position, Vector2f& areaSize);
};

