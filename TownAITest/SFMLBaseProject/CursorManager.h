#pragma once
#include "GraphicsManager.h"

enum CURSOR_TYPES
{
	NORMAL_CURSOR,
	CHOP_TREE_CURSOR,
	MINE_ROCK_CURSOR,
	ATTACK_CURSOR,
	BUILD_CURSOR,
	MAX_CURSOR_TYPES
};

class CursorManager : public IDrawable
{
private:
	static CursorManager* _instance;

	CURSOR_TYPES _cursorType;
	Sprite* _cursorSprites[CURSOR_TYPES::MAX_CURSOR_TYPES];

	RenderWindow* _gameWindow;

	CursorManager();
public:
	~CursorManager();

	void Init(RenderWindow* window);

	static CursorManager* Instance();

	void SetCursorType(CURSOR_TYPES newType)				{ _cursorType = newType; }

	void Draw() override;
};

