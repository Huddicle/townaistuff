#include "CursorManager.h"

CursorManager* CursorManager::_instance = nullptr;

CursorManager::CursorManager()
{

	_cursorType = CURSOR_TYPES::NORMAL_CURSOR;

	_cursorSprites[CURSOR_TYPES::NORMAL_CURSOR] = SFMLFactory::Instance()->MakeSprite("Resources/NormalCursor.png");
	_cursorSprites[CURSOR_TYPES::CHOP_TREE_CURSOR] = SFMLFactory::Instance()->MakeSprite("Resources/ChopTreeCursor.png");

	ShowCursor(false);
}


CursorManager::~CursorManager()
{
}

void CursorManager::Init(RenderWindow * window)
{
	_gameWindow = window;
}

CursorManager * CursorManager::Instance()
{
	if (_instance == nullptr)
	{
		_instance = new CursorManager();
	}

	return _instance;
}

void CursorManager::Draw()
{
	if (_cursorSprites[_cursorType] != nullptr && _gameWindow != nullptr)
	{
		_cursorSprites[_cursorType]->setPosition(Mouse::getPosition(*_gameWindow).x, Mouse::getPosition(*_gameWindow).y);

		GraphicsManager::Instance()->DrawSprite(*_cursorSprites[_cursorType]);

	}
}
