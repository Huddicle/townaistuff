#include "ChopTreeEvent.h"



ChopTreeEvent::ChopTreeEvent()
{
	_started = false;
	_startPosition = Vector2f(0.0f, 0.0f);
	_endPosition = Vector2f(0.0f, 0.0f);
}

ChopTreeEvent::~ChopTreeEvent()
{
}

void ChopTreeEvent::Start()
{
	_started = true;
	_startPosition.x = CollisionManager::Instance()->GetMousePosition().x;
	_startPosition.y = CollisionManager::Instance()->GetMousePosition().y;
}

void ChopTreeEvent::Cancel()
{
}

void ChopTreeEvent::Finish()
{
	CursorManager::Instance()->SetCursorType(CURSOR_TYPES::NORMAL_CURSOR);
	
	_endPosition.x = CollisionManager::Instance()->GetMousePosition().x;
	_endPosition.y = CollisionManager::Instance()->GetMousePosition().y;

	JobFactory::Instance()->CreateChopTreeGroupJob(_startPosition, _endPosition);

}

void ChopTreeEvent::Update(float t)
{

}

void ChopTreeEvent::Draw()
{
	if (_started)
	{
		RectangleShape rect = RectangleShape();
		rect.setFillColor(Color(50, 50, 200, 100));
		rect.setPosition(_startPosition);
		Vector2i currentMouse = CollisionManager::Instance()->GetMousePosition();
		rect.setSize(Vector2f(currentMouse.x - _startPosition.x, currentMouse.y - _startPosition.y));
		GraphicsManager::Instance()->DrawRectangle(rect);
	}

}
