#pragma once
#include "Map.h"

class JobFactory
{
private:
	static JobFactory* _instance;

	Map* _gameMap;

	JobFactory();

public:
	~JobFactory();

	void Init(Map* map);

	static JobFactory* Instance();

	void CreateChopTreeGroupJob(const Vector2f& startPosition, const Vector2f& endPosition);

};

