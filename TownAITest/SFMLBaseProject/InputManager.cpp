#include "InputManager.h"

InputManager* InputManager::_instance = nullptr;

InputManager::InputManager()
{
	_previousMouseDown = false;
	_previousMousePosition = Mouse::getPosition();
}

InputManager::~InputManager()
{
}

InputManager * InputManager::Instance()
{
	if (_instance == nullptr)
	{
		_instance = new InputManager();
	}

	return _instance;
}

void InputManager::Update(float t)
{
	bool mouseDown = Mouse::isButtonPressed(Mouse::Left);
	Vector2i mousePosition = Mouse::getPosition();

	if (mouseDown && !_previousMouseDown)
	{
		InputMessage* mouseDownMessage = new InputMessage();
		mouseDownMessage->type = MESSAGE_TYPE::INPUT_MESSAGE;
		mouseDownMessage->inputType = INPUT_MESSAGE_TYPE::MOUSE_DOWN;
		Notify(mouseDownMessage);
	}
	if (!mouseDown && _previousMouseDown)
	{
		InputMessage* mouseUpMessage = new InputMessage();
		mouseUpMessage->type = MESSAGE_TYPE::INPUT_MESSAGE;
		mouseUpMessage->inputType = INPUT_MESSAGE_TYPE::MOUSE_UP;
		Notify(mouseUpMessage);
	}

	_previousMouseDown = mouseDown;
	_previousMousePosition = mousePosition;
}
