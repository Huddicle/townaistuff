#pragma once

class IUserEvent : public IUpdatable, public IDrawable
{
protected:
	bool _started;
public:
	IUserEvent() {};
	~IUserEvent() {};

	virtual void Start() = 0;
	virtual void Cancel() = 0;
	virtual void Finish() = 0;

	virtual bool HasStarted() { return _started; }

	virtual void Update(float t) override = 0;
	virtual void Draw() override = 0;
};