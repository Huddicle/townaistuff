#ifndef _IUPDATABLE_H
#define _IUPDATABLE_H

class IUpdatable
{
public:
	IUpdatable() {};
	~IUpdatable() {};

	virtual void Update(float t) = 0;
};

#endif